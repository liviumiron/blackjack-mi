package ro.liviu.blackjack.enums;

public enum Endgame {
	VICTORY("Victory"),
	DEFEAT("Defeat"),
	DRAW("Draw"), 
	CONTINUE("Continue");
	
	
	private final String name;
    private Endgame(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
