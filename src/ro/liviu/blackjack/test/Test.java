package ro.liviu.blackjack.test;

import ro.liviu.blackjack.entities.DeckImpl;
import ro.liviu.blackjack.entities.HandImpl;
import ro.liviu.blackjack.interfaces.Deck;
import ro.liviu.blackjack.interfaces.Hand;

public class Test {

	
	public static void main(String... args) {
		
		
		testTurningAcesIntoOnes();
	}
	/**
	 *  Tests if the Hand makes the Aces have the value one if the total value of the hand exceeds 21
	 */
	private static void testTurningAcesIntoOnes() {
		Deck deck = new DeckImpl();
		
		Hand hand = new HandImpl("test");
		
		deck.getCard();
		
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		hand.addCard(deck.getCard());
		
		
		System.out.println("Hand:");
		hand.printHand();
		System.out.println(hand.getValue());
		
	}
	
	
}
