package ro.liviu.blackjack.interfaces;

public interface Deck {
	
	public void initialize();
	
	public Card getCard(boolean faceDown);
	
	public Card getCard();
	
	public void printDeck();
	
	public void shuffle();

}
